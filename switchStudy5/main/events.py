from .. import socketio, manager, logger
from . import main
from flask import session, request

import json
    
def action_callback(pos, uid):
    user = manager.get_user(uid)
    if not user:
        logger.debug("User not found, returning")
        return False
    
    msg = prepare_message(user)
    user.emit("update_map", msg)

def prepare_message(user, final=None):
    agentPos = user.agent.cur_pos

    vision = user.condition.vision 

    envMap = user.env.to_simple_list(agentPos)
    if vision == "memory":
        # Hide parts/books that have not been seen yet when we are in memory vision
        for i, row in enumerate(envMap):
            for j, tile in enumerate(row):
                if tile["part"] and (not user.has_seen((i,j)) or final):
                    tile["part"] = None

    gameMap = {"index": 0,
               "data": envMap}
    questions = user.get_questions(final)

    traj = user.agent.past_positions
    if final:
        traj = traj[:user.condition.query_points[-1]+1]
        agentPos = traj[-1]
    msg = {"map": gameMap, "agent":{"id": 0, "x": agentPos[1], "y":agentPos[0], "finished": user.agent.finished},
            "traj": traj, "show_questions": questions}
    return msg

@socketio.on('start_replay')
def start_session():
    logger.debug("Starting recording")
    user = manager.get_user(session["uid"])
    if not user:
        logger.debug("User not found, returning")
        return False

    user.agent.callback = action_callback
    user.agent.uid = session["uid"]
    user.agent.replay()
    

@socketio.on("get_last_questions")
def last_questions():
    logger.debug("Getting last questions")
    user = manager.get_user(session["uid"])
    if not user:
        logger.debug("User not found, returning")
        return False
    
    if user.agent.finished:
        logger.debug("agent finished, getting last question")

        msg = prepare_message(user, final=True)
        msg["agent"]["finished"] = False
        return msg 
    else:
        return None 

@socketio.on("finish_condition")
def finish_condition():
    logger.debug("Finish condition")
    user = manager.get_user(session["uid"])
    if not user:
        logger.debug("User not found, returning")
        return False
    
    if user.agent.finished:
        new_condition = user.setup_next_condition()
        if not new_condition:
            return "Finished"
        msg = prepare_message(user)
        return msg
    else:
        return False
    
@socketio.on('responses')
def handle_responses(responses):
    logger.debug("Received responses: {}".format(responses))    
    user = manager.get_user(session["uid"])
    if not user:
        logger.error("User not found while handling ratings, returning")
        return False
    
    user.record_responses(responses)
    if user.final_questions_asked == 1:
        return "Final_Question"
    return False if user.agent.finished else "QP_Questions" # Do we have more QPs within this recording?

@socketio.on('get_map')
def getMap():
    logger.debug("Get map request")
    user = manager.get_user(session["uid"])
    if not user:
        logger.debug("User not found, returning")
        return False
    user.sid = request.sid
    msg = prepare_message(user)
    return msg
    
@socketio.on("performStep")
def step():
    user = manager.get_user(session["uid"])
    if not user:
        logger.debug("User not found, returning")
        return False
    user.sid = request.sid
    agentPos = user.get_next_agent_position()
    
    if agentPos is None:
        return "Finished"
    msg = prepare_message(user)
    return msg

@socketio.on('connect')
def connect():
    try:
        logger.info("User: {} connected from {} (requestId: {})".format(session["uid"], request.remote_addr,request.sid))
    except KeyError:
        logger.info("Got a connection but no session uid")
    return 

@socketio.on('disconnect')
def on_disconnect(): 
    try:
        #Remove users that left, if they take to long to come back they'll have to start over.
        user = manager.get_user(session["uid"])
        logger.info("User: {} disconnected".format(request.sid))
        if user is None:
            #This user does not even exist anymore -> delete him
            del session["uid"]    
        else:
            #This user did not get a new sid in the meantime -> delete it
            if request.sid == user.sid and user.uid != manager.app.config["SECRET_ADMIN_USERNAME"]:
                del manager.users[session["uid"]]
                del session["uid"]    
        return
    except KeyError:
        #Disconnect was triggered by server itself so we do not need to perform
        # clean up
        return

# @socketio.on_error_default  # handles all namespaces without an explicit error handler
# def default_error_handler(e):
#     logger.error("Socket error: ", e)
