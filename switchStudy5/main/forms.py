#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 14:19:25 2017

@author: jpoeppel
"""

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email

class UsernamePasswordForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    
class EmailForm(FlaskForm):
    email = EmailField("Email address", [DataRequired(), Email()])
    
    
class CrowdForm(FlaskForm):
    crowdID = StringField('crowdID', validators=[DataRequired()])