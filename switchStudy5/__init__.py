#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thur Dez 13 14:11:23 2018

@author: jpoeppel
"""
try:
    import eventlet
    eventlet.monkey_patch()
except ImportError:
    pass

from flask import Flask
from flask_socketio import SocketIO
from werkzeug.middleware.proxy_fix import ProxyFix
# from werkzeug.contrib.fixers import ProxyFix

socketio = SocketIO() 
from .utils import Logger
logger = Logger()

from .experiment import Manager
manager = Manager()



def create_app(test_config=None):
    """
        Using the application factory pattern.
    """
    app = Flask(__name__, 
                instance_relative_config=True
                )
    app.config.from_object('config') #Will load config.py from root directory
    app.config.from_pyfile("config.py") #Will load instance config file
    app.debug = app.config["DEBUG"]

    app.wsgi_app = ProxyFix(app.wsgi_app)

    manager.app = app

    logger.set_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint, url_prefix=app.config["URL_PREFIX_SWITCH"])

    socketio.init_app(app, path=app.config["SWITCH_SOCKET_PATH"])

    return app
