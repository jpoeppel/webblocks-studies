var scaling = 0.6;
var lastMsg;
var show_vision = false;

function renderMap(canvas, map, cur_visibles) {
//    console.log("render map");
    context = canvas.getContext('2d');
    
    var canvasMaxWidth = window.innerWidth * scaling;
    var canvasMaxHeight = window.innerHeight * scaling;
    var tileSize = Math.min(Math.floor(canvasMaxWidth/map[0].length), Math.floor(canvasMaxHeight/map.length));
    canvas.width = tileSize*map[0].length;
    canvas.height = tileSize*map.length;
    context.clearRect(0, 0, canvas.width, canvas.height);    
    
    map.forEach(function(row,i) {
        row.forEach(function(tile, j) { 
            if (tile.color != "" || tile.part) {
                renderTile(context, tile, tileSize, j, i);
            }
        })
    })

    if (show_vision && cur_visibles) {
        context.fillStyle = "rgba(0,0,0,0.7)";
        context.fillRect(0, 0, canvas.width, canvas.height);    
        cur_visibles.forEach(function(el, i) {
            renderTile(context, el, tileSize, el.pos[1], el.pos[0])
        })
    }

    //The renderAgent function will need this information to correctly
    //size the agentCanvas.
    return [tileSize, canvas.width, canvas.height];
}

function renderTile(context, tile, tileSize, posX, posY) {
    if (tile.color !== "") {
        context.fillStyle = tile.color;
        context.fillRect(posX * tileSize, posY* tileSize, tileSize, tileSize);
    } else {
        context.fillStyle = "white";
        context.fillRect(posX * tileSize, posY* tileSize, tileSize, tileSize);
    }

    context.font = 0.8*tileSize +"px Ariel";
    context.fillStyle = "black";
    // context.fillText(tile.symbol, posX*tileSize+0.3*tileSize, posY*tileSize+0.8*tileSize);

    if (tile.part) {
        if (tile.part.symbol === "b") {
            drawBook(context, posX, posY, tileSize, tile.part.color);
        } else {
            drawKey(context, posX, posY, tileSize, tile.part.color);
        }
        
    }

    if (tile.lock) {
        drawLock(context, posX, posY, tileSize, tile);
    }
}

function drawBorders() {
    let canvas =  document.getElementById("agentcanvas");
    let context = canvas.getContext('2d');
    let tileSize = lastMsg.sizeInfo[0];
    let agent = lastMsg.agent;
    context.strokeStyle = "red";
    var posX, posY;
    let shifts = [[-1,0], [1,0], [0,-1], [0,1]];
    shifts.forEach( shift => {
        posX = agent.x + shift[0];
        posY = agent.y + shift[1];
        context.strokeRect(posX * tileSize, posY * tileSize, tileSize, tileSize);
    })

    // context.stroke();
}

function createPattern(color, width, dir) {
    var p = document.createElement("canvas")
    p.width=16;
    p.height=8;
    var pctx=p.getContext('2d');

    var x0=18;
    var x1=-2;
    var y0=-1;
    var y1=9;
    var offset=16;

    pctx.strokeStyle = color;
    pctx.lineWidth=width;
    pctx.beginPath();
    if (dir == 0) {
        pctx.moveTo(x0,y0);
        pctx.lineTo(x1,y1);
        pctx.moveTo(x0-offset,y0);
        pctx.lineTo(x1-offset,y1);
        pctx.moveTo(x0+offset,y0);
        pctx.lineTo(x1+offset,y1);
    } else {
        pctx.moveTo(x1,y0);
        pctx.lineTo(x0,y1);
        pctx.moveTo(x1-offset,y0);
        pctx.lineTo(x0-offset,y1);
        pctx.moveTo(x1+offset,y0);
        pctx.lineTo(x0+offset,y1);
    }
    
    pctx.stroke();

    return p;
}

function drawBinaryPatternTile(tileToDraw, copy) {
    let canvas =  document.getElementById("agentcanvas");
    let context = canvas.getContext('2d');
    let tileSize = lastMsg.sizeInfo[0];
    let agent = lastMsg.agent;
    const color = ["black", "brown"];
    const widths = [2,4];
    const shifts = {
        5: [[-1,0], [1,0]],
        3: [[0,-1], [1,0]],
        11: [[0,-1], [0,1]]
    }

    var p;
    var posX, posY, shift;
    for (var i=0; i<2; i++) {
        if (tileToDraw && tileToDraw == 3) {
            continue
        } else if (tileToDraw && tileToDraw < 3 && i > 0) {
            continue
        } else if (tileToDraw && tileToDraw > 3 && i < 1) {
            continue
        }
        p = createPattern(color[i], widths[i], i);
        context.fillStyle = context.createPattern(p, "repeat"); 
        context.strokeStyle = color[i];

        shift = shifts[agent.x][i];
        posX = agent.x + shift[0];
        posY = agent.y + shift[1];
        context.fillRect(posX * tileSize, posY * tileSize, tileSize, tileSize);
        context.strokeRect(posX * tileSize, posY * tileSize, tileSize, tileSize);
    }

    if (copy) {
        var tile = document.createElement("canvas");
        tile.width = tileSize;
        tile.height = tileSize;
        let ctx = tile.getContext("2d");
        ctx.fillStyle = ctx.createPattern(p, "repeat"); 
        ctx.fillRect(0,0,tileSize,tileSize);
        ctx.strokeRect(0,0, tileSize, tileSize);
        return tile;
    }
}

function drawSingleBorder(oldChoice) {
    let canvas =  document.getElementById("agentcanvas");
    let context = canvas.getContext('2d');
    let tileSize = lastMsg.sizeInfo[0];
    context.strokeStyle = "red";
    context.lineWidth = tileSize/10;
    let agent = lastMsg.agent;
    var posX, posY;
    switch (oldChoice) {
        case "Up":
            posX = agent.x;
            posY = agent.y - 1;
            break;
        case "Down":
            posX = agent.x;
            posY = agent.y + 1;
            break;
        case "Left":
            posX = agent.x - 1;
            posY = agent.y;
            break;
        case "Right":
            posX = agent.x + 1;
            posY = agent.y;
            break;
        case "I do not know":
            posX = agent.x;
            posY = agent.y;
            break;
    }
    context.strokeRect(posX * tileSize, posY * tileSize, tileSize, tileSize);
}

function drawKey(context, posX, posY, tileSize, color) {
    context.fillStyle = color;
    context.strokeStyle = color;
    centerX = posX*tileSize + 0.15*tileSize;
    centerY = posY*tileSize + 0.45*tileSize;
    context.fillRect(centerX, centerY, tileSize/2, tileSize/9);
    context.fillRect(centerX, centerY+tileSize/9, tileSize/20, tileSize/9);
    context.fillRect(centerX+tileSize/20, centerY+tileSize/9, tileSize/20, tileSize/18);
    context.fillRect(centerX+tileSize/10, centerY+tileSize/9, tileSize/20, tileSize/9);
    context.beginPath();
    context.arc(centerX+0.6*tileSize, centerY+tileSize/18, tileSize/6, 0, 2*Math.PI , false);
    context.fill();
    context.stroke();
}

function drawLock(context, posX, posY, tileSize, tile) {
    context.fillStyle = "lightgray";
    context.strokeStyle = "black";
    var sizeFactor = 9;
    centerX = posX*tileSize + 7.5*tileSize/sizeFactor;
    centerY = posY*tileSize + 1.8*tileSize/sizeFactor;
    var heightShift = 0;
    for (var i=0;i<tile.lock;i++) {
        // Draw the 3rd lock top left
        if (i==2) {
            centerX = posX*tileSize + 1.5*tileSize/sizeFactor;
            heightShift = 0;
        }
        // Draw arc
        context.beginPath();
        context.arc(centerX, centerY+heightShift, tileSize/sizeFactor, 0, Math.PI, true);
        context.stroke();
        // Draw body
        context.fillRect(centerX-tileSize/sizeFactor, centerY+heightShift, tileSize/(sizeFactor/2), tileSize/(sizeFactor/2));
        context.strokeRect(centerX-tileSize/sizeFactor, centerY+heightShift, tileSize/(sizeFactor/2), tileSize/(sizeFactor/2));
        heightShift += 2*tileSize/(sizeFactor/2);
    }
}

function drawBook(context, posX, posY, tileSize, color) {
    context.fillStyle = color;
    context.strokeStyle = color;
    centerX = posX*tileSize + 0.25*tileSize;
    centerY = posY*tileSize + 0.25*tileSize;
    context.fillRect(centerX, centerY, tileSize/2, tileSize/2);
    // context.fillRect(centerX, centerY+tileSize/9, tileSize/20, tileSize/9);
    // context.fillRect(centerX+tileSize/20, centerY+tileSize/9, tileSize/20, tileSize/18);
    // context.fillRect(centerX+tileSize/10, centerY+tileSize/9, tileSize/20, tileSize/9);
    // context.beginPath();
    // context.arc(centerX+0.6*tileSize, centerY+tileSize/18, tileSize/6, 0, 2*Math.PI , false);
    // context.fill();
    // context.stroke();
}

function renderAgent(canvas, sizeInfo, agentId, posX, posY) {
    tileSize = sizeInfo[0];
    if (canvas.width != sizeInfo[1]) {
        canvas.width = sizeInfo[1];
        canvas.height = sizeInfo[2];
    }
    var context = canvas.getContext("2d");
    context.strokeStyle = "black";
    context.beginPath();
    centerX = posX*tileSize + tileSize/2;
    centerY = posY*tileSize + tileSize/2;
    context.arc(centerX, centerY, 
                tileSize*0.4, 0, 2*Math.PI);
    context.fillStyle = "yellow";
    context.fill();
    context.stroke();
    
    eyeSize = tileSize/16;
    //Draw left eye
    context.fillStyle = "black";
    context.beginPath();
    context.arc(centerX-tileSize/7, centerY-tileSize/12, eyeSize, 0, 2*Math.PI);
    context.fill();
    context.stroke();
    
    //Draw right eye
    context.beginPath();
    context.arc(centerX+tileSize/7, centerY-tileSize/12, eyeSize, 0, 2*Math.PI);
    context.fill();
    context.stroke();
    
    //Draw mouth
    context.beginPath();
    context.arc(centerX, centerY, tileSize/5, Math.PI*5/6, Math.PI*1/6, true);
    context.stroke();
}

function renderPath(canvas, sizeInfo, traj) {
    let tileSize = sizeInfo[0];
    canvas.width = sizeInfo[1];
    canvas.height = sizeInfo[2];
    var context = canvas.getContext("2d");         
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.strokeStyle = "red";
    context.beginPath();
    traj.forEach(function(pos, i) {
        let centerX = pos[1]*tileSize + tileSize/2;
        let centerY = pos[0]*tileSize + tileSize/2;
        if (i == 0) {
            context.moveTo(centerX, centerY);   
        } else {
            context.lineTo(centerX,centerY);
        }
    })
    context.stroke();       
}

function renderParts(parts, sizeInfo) {
    var canvas =  document.getElementById("agentcanvas");
    var context = canvas.getContext('2d');
    var string = "Collected Keys: ";
    let tileSize = sizeInfo[0];
    var posY = 0; //sizeInfo[2]/tileSize - 1;
    context.font = 0.7*tileSize +"px Ariel";
    context.fillText(string, 0+0.2*tileSize, posY*tileSize+0.7*tileSize);
    var posX = 5;
    for (var i=0;i<parts.length;i++) {
        drawKey(context, posX+i, posY, tileSize, parts[i].color);
    }
}

window.onload = function() {
//    console.log("Setting focus");
    var input = document.body.focus();
    
    document.getElementById("game").addEventListener("touchstart", function(e) {
          var t2 = e.timeStamp;
          var t1 = e.currentTarget.dataset.lastTouch || t2;
          var dt = t2 - t1;
          var fingers = e.touches.length;
          e.currentTarget.dataset.lastTouch = t2;
        
          if (!dt || dt > 500 || fingers > 1) return; // not double-tap
        
          e.preventDefault();
          e.target.click();
});
}

var socket = null;
var lastQuestions = [];

if (window.addEventListener) {
    window.addEventListener("load", setUpSocket);
    window.addEventListener("resize", on_resize);
} else {
    window.attachEvent("onload", setUpSocket);   
    window.attachEvent("onresize", on_resize);  
}

//localStorage.debug = '*';
function setUpSocket() {
    console.log("Setup socket");
    socket = io.connect({"path":socketPath, "transports":["polling"]});
    
    socket.on("connect", function() {
        console.log("Connected");
        socket.emit("get_map", constructMap);
    });
    
    socket.on("construct_new_map", function(msg) {
       constructMap(msg);
       return;
    });
    socket.on("update_map", update_map);
    socket.on("disconnect", function() {
        console.log("Disconnected");
        return;
    });
    socket.on("finish", function(msg) {
        window.location.href = 'exit';
        return;
    });
}

function show_query() {
    var container = document.getElementById("container");
    container.classList.remove("hidden");

    var instructionDiv = document.getElementById("instructionDiv");
    instructionDiv.innerHTML = "";
}

function setup_questions(questions) {
    console.log("setting up questions: ", questions);
    lastQuestions = questions;
    var questionContainer = document.getElementById("questionContainer");
    // Clear question container 
    while (questionContainer.firstChild) {
        questionContainer.removeChild(questionContainer.firstChild);
    }

    for (var i=0; i< questions.length; i++) {
        var question = questions[i];
        var contDiv = document.createElement("div");
        contDiv.id = question.id + "_container";
        contDiv.classList = ["container"];
        var label = document.createElement("Label");
        label.id = question.id + "_label";
        label.setAttribute("for", question.id + "_input");
        label.innerHTML = question.question;
        label.classList.add("col1");
        contDiv.appendChild(label);
        switch (question.type) {
            case "text":
                var textInput = document.createElement("input");
                textInput.type = "text";
                textInput.id = question.id + "_input";
                textInput.name = question.id;
                textInput.size = 25;
                contDiv.appendChild(textInput);

                // Hard coded for prediction box
                if (question.id === "predictionExplanation") {
                    let oldChoice = question.oldChoice.split("__")[1];
                    console.log("oldChoice: ", oldChoice);
                    var tile = drawBinaryPatternTile(oldChoice, true);
                    var dTile = document.createElement("div");
                    dTile.appendChild(tile);

                    var oldString = label.innerHTML;
                    label.innerHTML = "";
                    var splits = oldString.split("__" + oldChoice + "__");

                    var d1 = document.createElement("div");
                    d1.innerHTML = splits[0];
                    label.appendChild(d1);
                    label.appendChild(dTile);
                    label.style.display = "block";

                    // label.innerHTML = label.innerHTML.replace("__" + oldChoice + "__", d.outerHTML);
                    //drawSingleBorder(oldChoice);
                }
                break;
            case "radio":
                var answerContainer = document.createElement("div");
                answerContainer.classList.add("col3");
                answerContainer.classList.add("row");
                question.options.forEach( (option,j) => {
                    var div = document.createElement("div");
                    var label = document.createElement("Label");
                    label.id = question.id + "_radioLabel" + j;
                    label.setAttribute("for", question.id + "_input" + j);
                    label.innerHTML = option;
                    var input = document.createElement("input");
                    input.type = "radio";
                    input.name = question.id;
                    input.id = question.id + "_input" + j;
                    input.value = option;
                    div.appendChild(input);
                    div.appendChild(label);
                    // input.appendChild(label);
                    // label.appendChild(input);
                    // answerContainer.appendChild(label);
                    answerContainer.appendChild(div);
                })
                contDiv.appendChild(answerContainer);
                break;
            case "binarylikert":
                    var answerContainer = document.createElement("div");
                    answerContainer.classList.add("col3");
                    answerContainer.classList.add("row");

                    question.options.forEach( (option,j) => {
                        var div = document.createElement("div");
                        var label = document.createElement("Label");
                        label.id = question.id + "_radioLabel" + j;
                        label.setAttribute("for", question.id + "_input" + j);

                        var div2 = document.createElement("div");

                        if (option.symbol) {
                            var s = option.symbol;
                            var pattern = createPattern(s.color, s.thickness, s.direction);
                            var pTile = document.createElement("canvas")
                            let tileSize = lastMsg.sizeInfo[0];
                            pTile.width=tileSize;
                            pTile.height=tileSize;
                            var pctx=pTile.getContext('2d');
                            pctx.fillStyle = context.createPattern(pattern, "repeat"); 
                            pctx.fillRect(0,0,tileSize,tileSize);
                            pctx.strokeStyle = s.color;
                            pctx.strokeRect(0,0,tileSize,tileSize);
                            div2.appendChild(pTile);
                        }

                        label.appendChild(div2);   
                        var div3 = document.createElement("div");
                        div3.innerHTML = option.text;
                        label.appendChild(div3); 
                        label.classList.add("col")
                        var input = document.createElement("input");
                        input.type = "radio";
                        input.name = question.id;
                        input.id = question.id + "_input" + j;
                        input.value = option.text;
                        input.number = j;
                        label.for = input.id;
                        div.appendChild(input);
                        div.appendChild(label);
                        // input.appendChild(label);
                        // label.appendChild(input);
                        // answerContainer.appendChild(label);
                        answerContainer.appendChild(div);
                    })
                    contDiv.appendChild(answerContainer);
                    drawBinaryPatternTile();
                    break;
            case "explanation":
                    var infoDiv = document.createElement("div");
                    infoDiv.classList.add("col3");
                    infoDiv.id = question.id + "_info";
                    infoDiv.innerHTML = "<i>" + question.info + "</i>";
                    var textInput = document.createElement("input");
                    textInput.type = "text";
                    textInput.id = question.id + "_input";
                    textInput.name = question.id;
                    textInput.classList.add("hidden");
                    contDiv.appendChild(infoDiv);
                    contDiv.appendChild(textInput);
                    drawBorders();
                    break;
        }
        questionContainer.appendChild(contDiv);
        if (i<questions.length-1) {
            questionContainer.appendChild(document.createElement("hr"))
        }
    }
}

function confirm() {
    var responses = {};
    var showReminder = false;
    for (var i=0; i< lastQuestions.length; i++) {
        var lastQuestion = lastQuestions[i];
        var questionInputs = document.getElementsByName(lastQuestion.id);
        for (var j = 0; j < questionInputs.length; j++) {
            var input = questionInputs[j];
            switch (input.type) {
                case "text":
                    responses[lastQuestion.id] = input.value;
                    break;
                case "radio":
                    if (input.checked) {
                        responses[lastQuestion.id] = input.value;
                        if (lastQuestion.type == "binarylikert") {
                            responses[lastQuestion.id] += "__" + input.number;
                        }
                    }
                    break
                case "explanation":
                    if (input.checked) {
                        responses[lastQuestion.id] = input.value;
                    }
            }
        }
        if (!responses[lastQuestion.id]) {
            document.getElementById(lastQuestion.id + "_container").classList.add("warn");
            showReminder = true;
        } else {
            document.getElementById(lastQuestion.id + "_container").classList.remove("warn");
        }
    }

    if (showReminder) {
        var check_reminder = document.getElementById("check_reminder");
        check_reminder.classList.remove("hidden");
    } else {
        var check_reminder = document.getElementById("check_reminder");
        check_reminder.classList.add("hidden");

        // Clear responses:
        for (var i=0; i< lastQuestions.length; i++) {
            var lastQuestion = lastQuestions[i];
            var questionInputs = document.getElementsByName(lastQuestion.id);
            for (var j = 0; j < questionInputs.length; j++) {
                var input = questionInputs[j];
                switch (input.type) {
                    case "text": 
                        input.value = "";
                        break 
                    case "radio":
                        input.checked = false;
                        break
                }
            }
        }
        socket.emit("responses", responses, prepare_next);
    }
}

function prepare_next(more_qps) {
    // Use this to clear border
    update_agentCanvas(lastMsg, lastMsg.sizeInfo);
    var container = document.getElementById("container");
    container.classList.add("hidden");
    if (more_qps === "QP_Questions") {
        var startBtn = document.getElementById("startBtn");
        startBtn.value = "Continue";
        startBtn.classList.remove("hidden");
        var instructionDiv = document.getElementById("instructionDiv");
        instructionDiv.classList.remove("hidden");
        instructionDiv.innerHTML = "Your answers have been recorded. You can continue the agent's recording by pressing the Continue button.";
    } else if (more_qps === "Final_Question") {
        socket.emit("get_last_questions", update_map);
    } else {
        // No more questions and we are done
        finish();
    }
}

function start() {
    var startBtn = document.getElementById("startBtn");
    socket.emit("start_replay");
    startBtn.classList.add("hidden");
    var instructionDiv = document.getElementById("instructionDiv");
    instructionDiv.classList.add("hidden");
}

function toggleAgentVision() {
    show_vision = !show_vision;
    update_map(lastMsg);
}

function constructMap(msg) {
    if (!msg) {
        window.location.href = 'exit';
        return;
    };
        
    lastMsg = msg;
    canvasDiv = document.getElementById("canvasDiv");
    
    var agentCanvas = document.getElementById("agentcanvas");
    if (!agentCanvas) {
        agentCanvas = document.createElement('canvas');
        agentCanvas.id = "agentcanvas";
        agentCanvas.className = "agentcanvas canvas";
        canvasDiv.appendChild(agentCanvas);        
    }
   
    var bgCanvas = document.getElementById("bgcanvas");
    if (!bgCanvas) {
       bgCanvas = document.createElement('canvas');
       bgCanvas.id = "bgcanvas";
       bgCanvas.className = "gamecanvas canvas";
       canvasDiv.appendChild(bgCanvas);
    }

    update_map(msg);
};        

function on_resize() {
    agentCanvas = document.getElementById("agentcanvas");
    if (agentCanvas) {
        update_map(lastMsg);   
    }
}


    
function prepare_finish() {
    var instructionDiv = document.getElementById("instructionDiv");
    instructionDiv.innerHTML = "Thank you for your answers. Before you are done with this study \
                                we have two more questions for you."
    var startBtn = document.getElementById("startBtn");
    startBtn.value = "Continue";
    startBtn.classList.remove("hidden");
    startBtn.onclick = () => get_last_questions();
}

function get_last_questions() {
    var startBtn = document.getElementById("startBtn");
    startBtn.value = "Continue";
    startBtn.classList.add("hidden");
    console.log("Getting last questions")
    socket.emit("get_last_questions", update_map)
}


function finish() {
    // If we want multiple conditions, load next condition from here
    socket.emit("finish_condition", update_map);
    var finishBtn = document.getElementById("finishBtn");
    finishBtn.classList.add("hidden");
    var instructionDiv = document.getElementById("instructionDiv");
    instructionDiv.innerHTML = "You will now see a different agent. <br> \
                                Please watch the recording carefully again so that you will be \
                                able to answer the questions regarding the agent's mental states. \
                                Press the Start button to begin."
    var startBtn = document.getElementById("startBtn");
    startBtn.value = "Start";
    startBtn.classList.remove("hidden");
    // window.location.href = 'exit';
    // return;
}

function onClick(event) {
    let canvas =  document.getElementById("agentcanvas");
    let r = canvas.getBoundingClientRect();
    var mouseX = event.clientX - r.left;
    var mouseY = event.clientY - r.top;
    let tileSize = lastMsg.sizeInfo[0];
    var col = Math.floor(mouseX / tileSize);
    var row = Math.floor(mouseY / tileSize);

    let agent = lastMsg.agent;
    let shifts = [[-1,0], [1,0], [0,-1], [0,1], [0,0]];
    shifts.forEach( (shift, idx) => {
        posX = agent.x + shift[0];
        posY = agent.y + shift[1];

        if (row == posY && col == posX) {
            console.log("pressed tile: ", row, col)
            update_agentCanvas(lastMsg, lastMsg.sizeInfo);
            drawBorders();
            let context = canvas.getContext('2d');
            context.strokeStyle = "red";
            context.lineWidth = tileSize/10;
            context.strokeRect(posX * tileSize, posY * tileSize, tileSize, tileSize);
            let predictionInput = document.getElementById("actionPrediction_input"); //Hard coded for now
            switch (idx) {
                case 0:
                    predictionInput.value = "Left";
                    break;
                case 1:
                    predictionInput.value = "Right";
                    break;
                case 2:
                    predictionInput.value = "Up";
                    break;
                case 3:
                    predictionInput.value = "Down";
                    break;
                case 4:
                    predictionInput.value = "I do not know";
                    break;
            }
        }
    });

}

function update_agentCanvas(msg, sizeInfo) {

    let agent = msg.agent;
    canvas =  document.getElementById("agentcanvas");
    canvas.addEventListener("click", onClick);
   
    if (msg.traj) {
       renderPath(canvas, sizeInfo, msg.traj);
    }
    renderAgent(canvas, sizeInfo, agent.id, agent.x, agent.y);
    if (msg.parts) {
        renderParts(msg.parts, sizeInfo);
    }
}

function update_map(msg) {
    if (msg === "Finished") {
        // socket.emit("next_condition", update_map);
        // socket.disconnect();
        window.location.href = 'exit';
        return;
    }

    let map = msg.map;
    lastMsg = msg;
    if (msg.agent.finished) {
       console.log("agent is finished;");
       prepare_finish();
    }
    let bgCanvas = document.getElementById("bgcanvas");
    var sizeInfo = renderMap(bgCanvas, map.data, map.cur_visible);  
    let canvasDiv = document.getElementById("canvasDiv");
    canvasDiv.style.height = bgCanvas.height + "px";
    canvasDiv.style.width = bgCanvas.width + "px";
    middleDiv = document.getElementById("middle");
    middleDiv.style.height = bgCanvas.height + "px";
   
    lastMsg.sizeInfo = sizeInfo;
    update_agentCanvas(msg, sizeInfo);
    console.log("received msg: ", msg);

    if (msg.show_questions && msg.show_questions.length > 0) {
        setup_questions(msg.show_questions);
        show_query();
    }
   
   return;    
}
