try:
    import eventlet
    eventlet.monkey_patch()
except ImportError:
    pass

from switchStudy4 import create_app, socketio

app = create_app()

if __name__ == "__main__":
    app.logger.debug("Starting socketio")
    socketio.run(app, host=app.config["HOST"], port=app.config["PORT"], debug=app.config["DEBUG"], use_reloader=False)
