# webblocks Study collection

This is a collection of my various online studies that I have run, starting form the original 
```webblocks``` study, which collected human navigation behavior in three different conditions
for the AAMAS paper. See a description of the different studies below.

## Requirements

In order to run the different studies you require the following python modules (possibly more):

* flask
* flask-socketio
* flask-wtf
* wtform

Recommended when actually hosted: 
* eventlet
* gunicorn

## Run locally

In order to run the different studies, you can simply use the ```run<Study>.py``` files and run them directly
with ```python```. This will start the development server build-in to flask which is fine for developing it locally.

## Run on webserver for actual study
To host it on an actual webserver, you probably want to setup a proper service that runs the server via gunicorn or
something similar.
An example service file for the webblocks service used on our experiments.scs server could look like this (saved to
```/etc/systemd/system/webblocks.service```):

```
[Unit]
Description=Gunicorn instance to serve webblocks
After=network.target

[Service]
User=web
Group=web
WorkingDirectory=/home/web/webblocks
Environment="PATH=/home/web/webblocks/webblocksenv/bin"
ExecStart=/home/web/webblocks/webblocksenv/bin/gunicorn --worker-class eventlet --workers 1 --bind unix:webblocks.sock -m 007 runWebblocks:app

[Install]
WantedBy=multi-user.target
```

You may also need to setup your webproxy/router accordingly so that requests are redirected to the
unix socket for both http(s) and websocket requests.

### instance/config.py

Apart from the general ```config.py``` where general, non-sensitive information can and should be configured, the flask application
will also try to load (and parse) the ```instance/config.py``` file, which should specify the following variables:
```
HOST = "0.0.0.0" # the host where to reach the server, localhost or 0.0.0.0 should usually be fine
PORT = 5005 # The port at which the server listens for incoming connections

SECRET_KEY = "secure secret key" # For production this should really be a good random secret key for session encryption!
SECRET_ADMIN_USERNAME = "" # Username and passwort for the admin account, only really required for the webblocks study
SECRET_ADMIN_PASSWORD = ""

CROWD_FLOWER_KEY = "" # The key crowdflower participants require to be reimbursed on the plattform 

# The different prefixes used by the different studies. If you only run a single one, you may not need to wory about
# this, but if you employ multiple apps on the same server and want to reach them from different urls you need to set
# them accordingly
URL_PREFIX_GOAL = ""
URL_PREFIX_WEB = ""
URL_PREFIX_SWITCH = ""
URL_PREFIX_HEURISTIC = ""
URL_PREFIX_INTERACTION = ""

# The websocket connection can be configured using the SWITCH_SOCKET_PATH variable in the instance/config.
SWITCH_SOCKET_PATH = ""
```

## Studies

A list and brief description of the different studies contained in this project.

### switchStudies

A study where we had participants rate the likelihood (in terms of likely/not likely) for each possible action
at various points of time within different trajectories from the original webblocks study. We further asked participants
to give an explanation for the trajectory they saw. 
The three variants included here differ in the conditions they tested and the way the question was asked.

