#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 14:37:20 2017

@author: jpoeppel
"""
from . import socketio, logger
from . import utils

from .blockworld import Environment

import uuid
import ast
import datetime
import random
import os, json
import copy
import threading

class Experiment(object):
    
    def __init__(self):
        self.recordingCounter = -1
        self.condition = random.choice([1,0])
        self.recordings = []
        
    def get_next_recording(self):
        self.recordingCounter += 1
        try:
            return self.recordings[self.recordingCounter]
        except IndexError:
            return None
        

class Condition(object):

    def __init__(self, id, map_string, exits, keys, query_points, vision, agent):
        self.id = id
        self.map_string = map_string
        self.exits = exits
        self.keys = keys
        self.query_points = query_points
        self.agent = agent
        self.vision = vision

    def desired_exit(self, desire):
        for _exit in self.exits:
            if _exit["symbol"] == desire:
                return _exit

    def required_key(self, desire):
        for key in self.keys:
            if key["corDoor"] == desire:
                return key

    def create_agent(self, environment):
        start_pos = tuple(self.agent["startPos"])
        behavior = dict(self.agent["behavior"])
        desire = behavior["desire"]
        if behavior["type"] == "optimal":
            req_key = self.required_key(desire)
            subgoals = [tuple(part["pos"]) for part in req_key["parts"]]
            behavior["subgoals"] = subgoals
            behavior["final_pos"] = tuple(self.desired_exit(desire)["pos"])
        elif behavior["type"] == "exploration":
            behavior["final_pos"] = tuple(self.desired_exit(desire)["pos"])
            req_key = self.required_key(desire)
            missing_parts = [(part["symbol"],req_key["color"]) for part in req_key["parts"]]
            behavior["initialBeliefs"]["missing_parts"] = missing_parts
            behavior["initialBeliefs"]["keyrooms"] = [tuple(room_pos) for room_pos in behavior["initialBeliefs"]["keyrooms"]]

        return Agent(environment, start_pos, behavior, self.query_points)

    @classmethod
    def from_file(cls, path):
        with open(path, "r") as f:
            condition_data = json.load(f)
        
        return cls(condition_data["id"], condition_data["map"], 
                    condition_data["exits"], condition_data["keys"],
                    condition_data.get("query_points", []),
                    condition_data.get("vision", "full"),
                    condition_data.get("agent", None))

class User(object):
    
    def __init__(self, remAdr, user_agent, conditions, user_number, sonaID = None, crowd = None, ):
        self.uid = str(uuid.uuid1())
        self.user_number = user_number
        self.remAdr = remAdr
        self.sid = -1
        self.logActions = True
        self.sonaID = sonaID
        self.crowdFlower = crowd
        self.crowdID = None
        self.email = ""
        self.user_agent = user_agent
        self.env = None
        self.action_counter = 0
        self.agent_behavior = None
        self.condition = None 
        self.conditions = list(conditions)
        self.next_condition_counter = 0
        self.finished = False
        self.collected_parts = []
        self.cur_ratings = 0
        self.final_questions_asked = 0
        self.resp_counter = 0
        self.third_response = None


        self.query_point_questions = [
            {
            "id": "actionPrediction",
            "question": "On which of the two highlighted tiles do you expect the agent to step next?",
            "type": "binarylikert",
            "options": [
                {"symbol": {"direction": 0, "thickness": 2, "color": "black"}, "text": "Certainly"}, 
                {"symbol": {"direction": 0, "thickness": 2, "color": "black"}, "text": "Very Likely"}, 
                {"symbol": {"direction": 0, "thickness": 2, "color": "black"}, "text": "Likely"}, 
                {"symbol": {"direction": 0, "thickness": 2, "color": "white"}, "text": "I do not know"}, 
                {"symbol": {"direction": 1, "thickness": 4, "color": "brown"}, "text": "Likely"}, 
                {"symbol": {"direction": 1, "thickness": 4, "color": "brown"}, "text": "Very Likely"}, 
                {"symbol": {"direction": 1, "thickness": 4, "color": "brown"}, "text": "Certainly"}, 
            ]
            }]

        self.final_questions_one = [
            {
            "id": "predictionExplanation",
            "question": "Please explain why you chose this when you predicted the agent's next move at this point: <br> {}",
            # "question": "Please explain why you chose '{}' when you predicted the agent's next move at this point:",
            "type": "text",
            "oldChoice": None
            }
        ]
        self.final_questions_two = [
            {
            "id": "desireKnowledge",
            "question": "Did you think the agent knew where the Blue book is located at this point?",
            "type": "radio",
            "options": [
                "Yes", "No", "I do not know"
                ]
            }
        ]

        condition_order = [con.id for con in conditions]
        if not "adm" in self.uid:
            if user_agent:
                userAgentInfo = "User_agent.platform: {}\nUser_agent.browser: {}\n"\
                            "User_agent.version: {}\nUser_agent.language: {}\n" \
                            "User_agent.string: {}".format(user_agent.platform,
                                                user_agent.browser,
                                                user_agent.version,
                                                user_agent.language,
                                                user_agent.string)
            else:
                userAgentInfo = "No info available"
            utils.log(self.uid, None, "Remote address: {}\nUser Agent Information:\n{}\nCondition order:{}\nSonaID: {}\nCrowdflower: {}\nUser number: {}\n"
                      .format(self.remAdr, userAgentInfo, condition_order, self.sonaID, self.crowdFlower, self.user_number), 
                      datetime.datetime.utcnow())

    def get_maze_nr(self):
        cond_id = self.condition.id 
        MAZES = {
            "1": "1",
            "1-2": "1",
            "1-3": "1-3",
            "1-4": "1-3",
            "2": "2",
            "2-2": "2",
            "2-3": "2-3",
            "2-4": "2-3"
        }
        logger.debug("Maze for condition {}: {}".format(self.condition.id, MAZES[cond_id]))
        return MAZES[cond_id]

    def has_seen(self, pos):
        # Just a bad hack relying on the fact that all trajectories are the same
        # and we only need to worry about the first location
        if pos == (1,1) and self.agent.action_counter > 15:
            return True 
        if self.agent.action_counter > len(self.agent.behavior["script"]) - 3:
            return True
        return False 

    def add_crowdID(self, crowdID):
        self.crowdID = crowdID
        utils.log(self.uid, None, "CrowdID: {}".format(crowdID), datetime.datetime.utcnow())
    def add_email(self, mail):
        self.email = mail
        utils.log(self.uid, None, "Email: {}".format(mail), datetime.datetime.utcnow())
    def add_sona_feedback(self, feedback):
        utils.log(self.uid, None, "Sona Feedback: {}".format(feedback), datetime.datetime.utcnow())
    @property    
    def is_authenticated(self):
        return self.uid == self.app.config["SECRET_ADMIN_USERNAME"]
    @property
    def is_active(self):
        return True
    @property
    def is_anonymous(self): 
        return self.uid != self.app.config["SECRET_ADMIN_USERNAME"]
    def get_id(self):
        return str(self.uid)
        
    def assign_condition(self, condition):
        self.condition = condition
        self.env = Environment()
        self.env.parse_environment(condition.map_string)
        self.env.set_targets(condition.exits)
        self.env.specify_keys(condition.keys)

        self.agent = condition.create_agent(self.env)

    def get_next_agent_position(self):
        return self.agent.perform_step()

    def get_questions(self, final=None):
        if final:
            if self.final_questions_asked == 0:
                return list(self.final_questions_one)
            elif self.final_questions_asked == 1:
                return list(self.final_questions_two)

        if self.agent.query_user():
            return list(self.query_point_questions)
        else:
            return []

    def record_responses(self, responses):
        cur_qp = self.agent.action_counter
        if self.agent.finished:
            self.final_questions_asked += 1
            
        self.resp_counter += 1
        if self.resp_counter == 3:
            logger.debug("Old action prediction response: {}".format(responses["actionPrediction"]))
            question = self.final_questions_one[0]
            question["question"] = question["question"].format(responses["actionPrediction"].split("__")[0] + "__" + responses["actionPrediction"].split("__")[1] + "__")
            question["oldChoice"] = responses["actionPrediction"]
            self.final_questions_one = [question]
            logger.debug("Fixing final questions: {}".format(self.final_questions_one))


        utils.log(self.uid, 
                self.condition, 
                "Step: {}, Pos: {}, Responses: {}".format(cur_qp, self.agent.cur_pos, responses),
                datetime.datetime.utcnow())

    def setup_next_condition(self):
        if self.next_condition_counter < len(self.conditions):
            self.assign_condition(self.conditions[self.next_condition_counter])
            self.next_condition_counter += 1
            return self.condition
        else:
            self.finished = True
            return None
 
    def emit(self, event, data):
        socketio.emit(event, data, room=self.sid)
        
class Agent(object):

    def __init__(self, environment, start_pos, behavior, stop_points):
        self.env = environment
        self.behavior = behavior 
        self.cur_pos = start_pos
        self.past_positions = [start_pos]
        self.action_counter = 0
        self.stop_points = list(stop_points)
        self.finished = False
        self.collected_parts = []
        self.cur_traj = None
        self.beliefs = None
        self.explored_rooms = []
        self.timer = None
        self.callback = None

    def get_all_positions(self, get_keys=False):
        positions = [self.cur_pos]
        keys = [()]
        done = False
        while not done:
            new_pos = self.perform_step()
            keys.append(list(self.collected_parts))
            if new_pos is None:
                done = True 
            else:
                positions.append(new_pos)
        if get_keys:
            return positions, keys
        return positions

    def replay(self, interval=0.3):
        action = self.perform_step()
        self.callback(action, self.uid)
        if action is not None:
            if self.timer:
                self.timer.cancel()
            if not self.action_counter in self.stop_points:
                self.timer = threading.Timer(interval, self.replay)
                self.timer.start()

    def stop_replay(self):
        if self.timer:
            self.timer.cancel()

    def query_user(self):
        return self.action_counter in self.stop_points

    def perform_step(self):
        action = None
        if self.behavior["type"] == "scripted":
            action = self.perform_scripted_action()
        elif self.behavior["type"] == "optimal":
            action = self.perform_optimal_action()
        elif self.behavior["type"] == "exploration":
            action = self.perform_exploration_action()
        else:
            logger.error("Unknown behavior type: {}".format(self.behavior["type"]))
        if action is not None:
            self.action_counter += 1
        return action
        

    def _setup_initial_beliefs(self):
        self.beliefs = dict(self.behavior["initialBeliefs"])

    def perform_exploration_action(self):
        if self.beliefs is None:
            self._setup_initial_beliefs()

        if self.cur_traj is None and self.finished:
            # Signale that we are done
            return None
        
        if self.beliefs["missing_parts"]:
            # explore keyrooms for missing parts
            if self.cur_pos in self.beliefs["keyrooms"] and self.cur_pos not in self.explored_rooms:
                # Are we currently standing at the entrance of a keyroom?
                self.explored_rooms.append(self.cur_pos)
                seen_objects = self.env.get_visible_objects(self.cur_pos, radius=3)
                objects_to_collect = []
                for obj in seen_objects:
                    if obj.part:
                        if (obj.part["symbol"], obj.part["color"]) in self.beliefs["missing_parts"]:
                            objects_to_collect.append(obj)

                if objects_to_collect:
                    # Overwrite cur_traj to pickup our symbols in this room
                    self.cur_traj = self.env.compute_optimal_traj(self.cur_pos, self.cur_pos, 
                                                            [obj.pos for obj in objects_to_collect])
                    for obj in objects_to_collect:
                        self.beliefs["missing_parts"].remove((obj.part["symbol"], obj.part["color"]) )

        if self.cur_traj is None:
            if self.beliefs["missing_parts"]:
                # Find next keyroom
                min_dist = 10000
                min_traj = None
                keyrooms_to_check = [room for room in self.beliefs["keyrooms"] if not room in self.explored_rooms]
                self.cur_traj  = self.env.compute_optimal_traj(self.cur_pos, self.behavior["final_pos"], keyrooms_to_check)
            else:
                # We are not missing parts anymore
                traj, d = self.env.get_shortest_trajectory(self.cur_pos, self.behavior["final_pos"])
                self.cur_traj = traj[1:]
            
        new_pos = self.cur_traj.pop(0)
        self._handle_new_pos(new_pos)
        if len(self.cur_traj) == 0:
            # We might need to find a new trajectory at the next step
            self.cur_traj = None
        return self.cur_pos
            
    def perform_optimal_action(self):
        if self.cur_traj is None:
            sub_goals = self.behavior["subgoals"]
            desired_exit = tuple(self.behavior["final_pos"])
            self.cur_traj = self.env.compute_optimal_traj(self.cur_pos, desired_exit, sub_goals)
        if len(self.cur_traj) == 0:
            return None
        new_pos = self.cur_traj.pop(0)
        self._handle_new_pos(new_pos)
        return self.cur_pos      

    def perform_scripted_action(self):
        if self.action_counter < len(self.behavior["script"]):
            next_action = self.behavior["script"][self.action_counter]
            return self.perform_action(next_action)
        else:
            logger.debug("No more scripted actions")
            return None

    def script_done(self):
        return self.action_counter >= len(self.behavior["script"])

    def _handle_new_pos(self, new_pos):
        hit_tile = self.env.tiles[new_pos]
        if hit_tile.passable:
            self.cur_pos = new_pos
            self.past_positions.append(new_pos)
        if hit_tile.part:
            # Collect tile part
            self.collected_parts.append(hit_tile.part)
            # hit_tile.part = None
        if hit_tile.is_target:
            # Check if all keyparts have been collected
            logger.debug("Reached desired exit")
            if hit_tile.opens(self.collected_parts):
                logger.debug("Door opened")
                if str(hit_tile.symbol) == str(self.behavior["desire"]):
                    logger.debug("Door is also agent's desire: finished")
                    self.finished = True
            else:
                logger.debug("Door is closed, maybe some parts are still missing?")
            
            self.collected_parts = [p for p in self.collected_parts if not p in hit_tile.present_parts]
            # for p in hit_tile.present_parts:
            #     if p in self.collected_parts:
            #         self.collected_parts.remove(p)

    def perform_action(self, action):
        ACTION_MAP = {"Up": (-1,0), "Down": (1,0), "Left": (0,-1), "Right": (0,1)}
        delta = ACTION_MAP[action]
        cur_pos = self.cur_pos
        new_pos = (cur_pos[0] + delta[0], cur_pos[1] + delta[1])
        self._handle_new_pos(new_pos)
        return self.cur_pos 

class Manager(object):
    
    def __init__(self):
        self.users = {}
        self.conditions = self.load_conditions_from_file()  
        self.condition_counter = 0
        
    def load_conditions_from_file(self):
        path = "switchingStudyConditions/costManipulation2/"
        allFiles = os.listdir(path)
        files = [f for f in allFiles if os.path.isfile(path + f)
                                and not "~" in f and not "_" in f and not "0" in f]
        sortedFiles = sorted(files)
        conditions = []
        for f in sortedFiles:
            condition = Condition.from_file(path+f)
            conditions.append(condition)
        return conditions
        
    def new_user(self, remAdr, user_agent, sonaID = None, crowd=None, debug=False):
        user_conditions = [self.conditions[self.condition_counter % len(self.conditions)]]
        self.condition_counter += 1

        user = User(remAdr, user_agent, user_conditions, self.condition_counter, sonaID= sonaID, crowd=crowd)
        user.app = self.app
        if debug:
            user.logActions = False
        user.setup_next_condition()

        self.users[user.uid] = user
        return user
    
    def get_user(self, uid):
        try:
            return self.users[str(uid)]
        except KeyError:
            logger.debug("No user for uid: {}".format(uid))
            return None
        
        
