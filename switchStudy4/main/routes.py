from flask import render_template, current_app, session, redirect, request, url_for
from . import main
from .forms import CrowdForm
from .. import manager

@main.route("/updateBrowser")
def update_browser():
    # remove user with incompatible browser
    del manager.users[session["uid"]]
    del session["uid"]
    return render_template("updateBrowser.html")

@main.route('/') 
@main.route('/index')
def index():
    #Check if we already know this user
    current_app.logger.debug("New user without crowd.")
    session["crowd"] = False
    try:
        user = manager.get_user(session["uid"])
        if not user:
            #This should not happen
            current_app.logger.debug("No user for existing session uid ({})".format(session["uid"]))
            #In this case we will create a new one anyways and overwrite the
            #session uid
            user = manager.new_user(request.environ['REMOTE_ADDR'],
                                        user_agent=request.user_agent, crowd=False)
            session["uid"] = user.uid
        # elif user.startedGame and not user.isDone:
            # return redirect(url_for(".game"))
    except KeyError:
        #Session does not have a user id, we need to assume that he is new 
        #or doing it a second time
        user = manager.new_user(request.environ['REMOTE_ADDR'],
                                    user_agent=request.user_agent, crowd=False)
        session["uid"] = user.uid
    
    current_app.logger.debug("User unknown, go back to start.")
    return render_template('index.html', title='Home', user=user, mazeNr=user.get_maze_nr())

@main.route("/crowdStudy", methods=['GET', 'POST'])
def study():
    #Check if we already know this user
    current_app.logger.debug("New user with crowd.")
    session["crowd"] = True
    try:
        user = manager.get_user(session["uid"])
        if not user:
            #This should not happen
            current_app.logger.debug("No user for existing session uid ({})".format(session["uid"]))
            #In this case we will create a new one anyways and overwrite the
            #session uid
            user = manager.new_user(request.environ['REMOTE_ADDR'],
                                        user_agent=request.user_agent, crowd=True)
            session["uid"] = user.uid
        elif not user.crowdFlower:
            del manager.users[session["uid"]] 
            user = manager.new_user(request.environ['REMOTE_ADDR'],
                                        user_agent=request.user_agent, crowd=True)
            session["uid"] = user.uid
        elif user.resp_counter > 0:
            return redirect(url_for(".game"))
    except KeyError:
        #Session does not have a user id, we need to assume that he is new 
        #or doing it a second time
        user = manager.new_user(request.environ['REMOTE_ADDR'],
                                    user_agent=request.user_agent, crowd=True)
        session["uid"] = user.uid
    
    # current_app.logger.debug("User unknown, go back to start.")

    if user.crowdFlower:
        form = CrowdForm()
        if form.validate_on_submit():
            user.add_crowdID(form.crowdID.data)
            current_app.logger.debug("Crowd id added to user {}".format(user.uid))

            key = "{}{}{}{}".format(user.crowdID[-1], current_app.config["CROWD_FLOWER_KEY"], user.user_number, user.crowdID[0])
            return render_template("study.html", socket_path=current_app.config["SWITCH_SOCKET_PATH"], start_hidden=user.agent.timer != None)
        return render_template("index.html", title="Home", user=user, mazeNr=user.get_maze_nr(), form=form)

    return render_template('index.html', title='Home', user=user, mazeNr=user.get_maze_nr())

@main.route('/game')
def game():
    try:
        user = manager.get_user(session["uid"])
    except KeyError:
        current_app.logger.debug("User id not found, redirect")
        return redirect(url_for(".index"))
    if not user:
        current_app.logger.debug("User was None (should not happen), redirect")
        return redirect(url_for(".index"))
    
    start_hidden = user.agent.timer != None
    current_app.logger.debug("Start hidden: {}".format(start_hidden))
    return render_template('study.html', socket_path=current_app.config["SWITCH_SOCKET_PATH"], start_hidden=start_hidden)

@main.route('/exit', methods=['GET', 'POST'])
def bye():
    current_app.logger.debug("Start of exit route")
    try:
        user = manager.get_user(session["uid"])
    except KeyError:
        current_app.logger.debug("Key error when getting current user in exit")
        if "crowd" in session and session["crowd"]:
            return redirect(url_for(".study"))
        return redirect(url_for(".index"))
    else:
        if user is None:
            current_app.logger.debug("User is None")
            if "crowd" in session and session["crowd"]:
                return redirect(url_for(".study"))
            return redirect(url_for(".index"))
        if user.finished and user.uid != current_app.config["SECRET_ADMIN_USERNAME"]:
            if user.crowdFlower:
                form = CrowdForm()
                if user.crowdID or form.validate_on_submit():
                    if not user.crowdID:
                        user.add_crowdID(form.crowdID.data)
                    current_app.logger.debug("Crowd id added to user {}".format(user.uid))
                    del manager.users[session["uid"]]
                    del session["uid"]
#                    disconnect()
                    current_app.logger.debug("crowdID: {}".format(user.crowdID))
                    current_app.logger.debug("user_number: {}".format(user.user_number))
                    key = "{}{}{}{}".format(user.crowdID[-1], current_app.config["CROWD_FLOWER_KEY"], user.user_number, user.crowdID[0])
                    
                    return render_template("exit.html", user=user, form=None, added=True, crowdFlowerKey=key)
                return render_template("exit.html", user=user, form=form, added=False)
            else:
                # Clean up normal users where we do not need to perform anything
                # else.
                del manager.users[session["uid"]]
                del session["uid"]
#                disconnect()
                return render_template("exit.html", user=user, form=None)

        return redirect(url_for(".game"))