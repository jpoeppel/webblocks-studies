#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 13:44:52 2017

@author: jpoeppel
"""

import os
import queue
import threading
import time
import urllib.request

colors = {"a": "", "g": "", "#": "red", "t": "green"}

passables = {"a": True, "g": True, "#": False, "t": True}

logQueue = queue.Queue()

study_name = os.path.split(os.path.dirname(os.path.realpath(__file__)))[1]
study_name = study_name[0].capitalize() + study_name[1:]
PREFIX = "results" + study_name

def writeLog():
    while True:
        item = logQueue.get()
        if item is None:
            time.sleep(0.1)
            continue
        userId, condition, msg, timestamp = item
        #Check if there already is a directory for this user:
        prefix = PREFIX
        dirPath = prefix + os.path.sep + str(userId)
        if not os.path.isdir(dirPath):
            os.makedirs(dirPath)
        if condition is not None:
            with open(dirPath+os.path.sep+"rec"+str(condition.id), "a") as f:
                f.write("{}: {}\n".format(timestamp, msg))
        else:
            with open(dirPath+os.path.sep+"userInfo", "a") as f:
                f.write("{}: {}\n".format(timestamp, msg))
        
logThread = threading.Thread(target=writeLog)
logThread.setDaemon(True)
logThread.start()
def log(userId, condition, msg, timestamp):
    logQueue.put((userId,condition, msg, timestamp))
        
        
def notify_sona_server(user, sona_server_url_template):
    
    def _threaded_notify():
        feedback = urllib.request.urlopen(sona_server_url_template.format(user.sonaID)).read()
        user.add_sona_feedback(feedback)
    
    t = threading.Thread(target=_threaded_notify())
    t.setDaemon(True)
    t.start()


class Logger(object):

    def __init__(self):
        self.app = None
        self.silent = False

    def set_app(self, app):
        self.app = app

    def dummy_print(self, text):
        if not self.silent:
            print(text)

    def __getattr__(self, name):
        if self.app:
            return self.app.logger.__getattribute__(name)
        else:
            return self.dummy_print